# Dash-blocks – Make dashboards with YAML files!

Make dashboards that read data sources from various formats (CSV, XLSX, ODS, SQLite, and more), run SQL queries and display metrics in blocks, all defined in a YAML file. We believe that dashboards can be expressed in a declarative way, and let a runtime engine do the wiring.

Dash-blocks dashboards are fully interactive and reactive user interfaces, despite the fact that they are defined in a YAML file, thanks to [Dash](https://dash.plot.ly/) in particular.

Minimal example:

```YAML
title: My first dashboard
sources:
  persons: persons.csv
blocks:
  - title: Nb persons
    type: big_text
    query: SELECT COUNT(*) FROM persons
```

For a more complex example, see [`dashboard.yml`](./dashboard.yml).

## Demo on Heroku

https://dash-blocks.herokuapp.com/

## Installation

```bash
mkvirtualenv dash-blocks
pip install -e .
```

## Usage

In development:

```bash
export FLASK_ENV=development
dash-blocks --log debug --debug dashboard.yml
# or
python3 dash_blocks/cli.py --log debug --debug dashboard.yml
```

Open http://127.0.0.1:5000/

### Specifying database URL

If you'd like to write the SQLite file or use another database engine, just use the `--database-url` option:

```bash
dash-blocks --database-url my_database.sqlite dashboard.yml
```

The only constraint is that the database URL must be compatible with SQLAlchemy (see [its docs](https://docs.sqlalchemy.org/en/latest/core/engines.html#sqlalchemy.create_engine)).

## Code quality

```bash
pylint *.py dash_blocks
```

Note: install pylint inside your virtualenv.
from pathlib import Path

from dash_blocks.app import create_app

sqlite_file = Path("dashboard.sqlite")
if sqlite_file.is_file():
    sqlite_file.unlink()
app = create_app(dashboard_file=Path('dashboard.yml'), database_url='sqlite:///dashboard.sqlite')
server = app.server  # Referenced by Procfile

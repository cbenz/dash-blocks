from setuptools import setup

setup(
    name='dash-blocks',
    version='0.0.1',
    description='Make dashboards with YAML files!',
    url='https://framagit.org/cbenz/dash-blocks',
    author='Christophe Benz',
    author_email='christophe.benz@jailbreak.paris',
    license='AGPLv3',
    packages=['dash_blocks'],
    zip_safe=False,
    install_requires=[
        'dash',
        'dash_core_components',
        'dash_html_components',
        'dash',
        'dataset',
        'importlib_resources',
        'jsonpointer',
        'jsonschema',
        'plotly',
        'python-slugify',
        'pyyaml',
        'tableschema',
        'tableschema-sql',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
    entry_points={
        'console_scripts': [
            'dash-blocks = dash_blocks.cli:main',
        ]
    },
)

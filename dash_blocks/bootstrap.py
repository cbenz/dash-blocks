import dash_html_components as html

"""Python functions helping to create Bootstrap 4 components."""


def classNames(*args, **kwargs):
    """Extract "className" from `kwargs` and return a className `str` with `classNames` and the extracted value.

    >>> classNames("container")
    ('container', {})
    >>> classNames("container", className="a")
    ('container a', {})
    >>> classNames("container", className="a", x="y")
    ('container a', {'x': 'y'})
    """
    className = kwargs.pop("className", None)
    if className is not None:
        args = list(args) + [className]
    return (" ".join(args), kwargs)


def container(children, fluid=False, **kwargs):
    className, kwargs = classNames("container-fluid" if fluid else "container", **kwargs)
    return html.Div(children, className=className, **kwargs)


def card(children, title=None, subtitle=None, links=None, **kwargs):
    className, kwargs = classNames("card", **kwargs)
    return html.Div(className=className, children=[
        html.Div(className="card-body d-flex flex-column", children=[
            html.H5(title, className="card-title") if title is not None else None,
            html.H6(subtitle, className="card-subtitle mb-2 text-muted") if subtitle is not None else None,
            html.Div(className="my-auto", children=children),
            [
                html.A(text, href=href, className="card-link")
                for href, text in links
            ] if links is not None else None,
        ]),
    ], **kwargs)

import dash_core_components as dcc
import dash_html_components as html

"""Blocks functions return `dash_html_components` values."""


def big_text(value, id):
    return html.Div(value, id=id, className="display-1 text-center")


def graph(rows, id, **options):
    return dcc.Graph(
        id=id,
        figure=graph_figure(rows, **options),
    )


def graph_figure(rows, x, y, marker={}, layout={}, type=None, **options):
    return {
        'data': [
            {
                'x': [row[x] for row in rows],
                'y': [row[y] for row in rows],
                'type': type,
                'marker': marker,
            },
        ],
        'layout': layout,
    }


def list_ul(rows, id, **options):
    return html.Ul(id=id, children=list_ul_children(rows, **options))


def list_ul_children(rows, column_names=False, **options):
    return [
        html.Li(", ".join(
            ("{}: {}".format(k, v) for k, v in row.items())
            if column_names
            else map(str, row.values())
        ))
        for row in rows
    ]


def range_slider(value, id, min, max):
    return html.Div([
        dcc.RangeSlider(
            className="my-1",
            id=id,
            min=min,
            max=max,
            value=value,
        ),
        html.P("", id="{}__label".format(id)),
    ])


def update_range_slider_label(value):
    return "{} - {}".format(*value)


def table(rows, id, table_classes="", **options):
    return html.Div(className="table-responsive", children=[
        html.Table(id=id, className="table table-hover table-sm " + table_classes,
                   children=table_children(rows, **options))
    ])


def table_children(rows, header=True, **options):
    column_names = list(rows[0].keys())
    return [
        html.Thead(children=[
            html.Tr([
                    html.Th(scope="col", children=column_name)
                    for column_name in column_names
                    ])
        ]) if header else None,
        html.Tbody(children=[
            html.Tr([
                    html.Td(cell)
                    for cell in row.values()
                    ])
            for row in rows
        ])
    ]


metadata_by_type = {
    "big_text": {
        "render": big_text,
        "callback_output_component_property": "children",
        "result_type": "value",
    },
    "graph": {
        "render": graph,
        "render_callback_result": graph_figure,
        "callback_output_component_property": "figure",
        "result_type": "rows",
    },
    "list_ul": {
        "render": list_ul,
        "render_callback_result": list_ul_children,
        "callback_output_component_property": "children",
        "result_type": "rows",
    },
    "range_slider": {
        "render": range_slider,
        "callback_input_component_property": "value",
        "callback_output_component_property": "children",
        "callbacks": [update_range_slider_label],
    },
    "table": {
        "render": table,
        "render_callback_result": table_children,
        "callback_output_component_property": "children",
        "result_type": "rows"
    },
}

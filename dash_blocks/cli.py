#!/usr/bin/env python3


"""Dash-blocks"""

import argparse
import logging
import sys
from pathlib import Path

import importlib_resources

import dash_blocks
from dash_blocks.app import create_app


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('dashboard_file', type=Path, help='path of the dashboard YAML file')
    parser.add_argument('--database-url', default="sqlite:///dashboard.sqlite", help='any valid SQLAlchemy engine URL')
    parser.add_argument('--debug', action='store_true', help='reload server when code changes (see Flask debug mode)')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    parser.add_argument('--port', default=5000, help='the port of the webserver')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(message)s",
        level=numeric_level,
        stream=sys.stderr,  # script outputs data
    )

    app = create_app(dashboard_file=args.dashboard_file, database_url=args.database_url)
    app.run_server(
        port=args.port,
        debug=args.debug,
        # Reload server if dashboard file or schema changes.
        extra_files=[
            str(args.dashboard_file.absolute()),
            str(importlib_resources.path(dash_blocks, 'dashboard_schema.json')),
        ],
    )

    # Disable reloader to avoid loading app twice, which causes SQLite DB locking errors.
    # app.run_server(port=args.port, debug=args.debug, use_reloader=False)


if __name__ == '__main__':
    main()

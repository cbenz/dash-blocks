import json
import logging
import re
from pathlib import Path

import dash
import dash_html_components as html
import dataset
import importlib_resources
import jsonschema
import sqlalchemy
import tableschema
import yaml
from dash.dependencies import Input, Output
from jsonpointer import resolve_pointer
from slugify import slugify

from . import blocks, bootstrap

log = logging.getLogger(__name__)
query_params_reference_regex = re.compile(r"^\$([^/]+)(.+)$")

with importlib_resources.open_binary('dash_blocks', 'dashboard_schema.json') as fp:
    dashboard_schema = json.load(fp)
# Patch JSON schema to restrict the possible values for a block type.
dashboard_schema['definitions']['block']['properties']['type']['enum'] = sorted(blocks.metadata_by_type.keys())


def load_dashboard(file_path):
    with file_path.open() as fp:
        dashboard_spec = yaml.load(fp)
    validator = jsonschema.validators.Draft4Validator(dashboard_schema)
    errors = sorted(validator.iter_errors(dashboard_spec), key=lambda e: e.path)
    return dashboard_spec, errors


def load_source(db, source, target):
    table = tableschema.Table(source)
    table.infer()
    table.save(target, storage='sql', engine=db.engine)


def load_block(db, block_spec, filters_infos, filter_block_spec_by_id):
    block_id = block_spec['id']
    block_type = block_spec['type']
    block_metadata = blocks.metadata_by_type[block_type]

    block_value = None
    card_subtitle = None
    query = block_spec.get('query')
    if query is None:
        block_value = block_spec.get('value')
    else:
        resolved_query_params = {}
        # Resolve query params which are filters referenced by pointers.
        if filters_infos:
            filters_blocks_titles = []
            for filter_block_id in sorted(filters_infos):
                query_params = filters_infos[filter_block_id]
                filter_block_spec = filter_block_spec_by_id[filter_block_id]
                filters_blocks_titles.append(filter_block_spec["title"])
                for query_param_name, pointer in query_params.items():
                    resolved_query_params[query_param_name] = resolve_pointer(filter_block_spec, pointer)

            card_subtitle = 'filtered by {}'.format(", ".join('"{}"'.format(filter_block_title)
                                                              for filter_block_title in filters_blocks_titles))

        # Resolve query params which are not filters.
        for query_param_name, query_param in block_spec.get('query_params', {}).items():
            if query_param_name not in resolved_query_params:
                resolved_query_params[query_param_name] = query_param

        try:
            result = run_query(db, query, resolved_query_params, block_id)
        except (sqlalchemy.exc.OperationalError, sqlalchemy.exc.StatementError) as exc:
            log.exception(exc)
            return bootstrap.card(className="border-danger", title=block_spec.get("title"), children=[
                html.Code(exc.statement.strip()),
                html.Ul([
                        html.Li(arg)
                        for arg in exc.args
                        ]) if len(exc.args) > 1
                else html.P(exc.args[0], className="card-text")
                if len(exc.args) == 1
                else html.P(str(exc), className="card-text"),
            ])

        result_type = block_metadata["result_type"]
        block_value = prepare_block_value(result, result_type)
    log.debug("Passing value %r to block %r", block_value, block_id)

    block_options = block_spec.get("options", {})
    render_func = block_metadata["render"]
    return bootstrap.card(className="h-100", title=block_spec.get("title"), subtitle=card_subtitle,
                          children=[render_func(block_value, block_spec['id'], **block_options)])


def is_block_filter(block_spec):
    return 'id' in block_spec and 'query' not in block_spec


def prepare_block_value(result, result_type):
    """Transform query `result` to a value processable by a block, based on `result_type`."""
    if result_type == 'value':
        row = next(result)
        return next(iter(row.values()))
    elif result_type == 'list':
        return [next(iter(row.values())) for row in result]
    elif result_type == 'rows':
        return list(result)
    else:
        raise ValueError("Unexpected value for 'result_type': {!r}".format(result_type))


def get_block_initial_value(block_spec):
    block_type = block_spec["type"]
    if block_type == "range_slider":
        block_options = block_spec.get("options", {})
        return [block_options["min"], block_options["max"]]
    else:
        raise NotImplementedError(block_spec)


def run_query(db, query, query_params, block_id):
    log.debug("Running query %r with params %r for block %r", query, query_params, block_id)
    return db.query(query, **query_params)


def make_callback(block_spec, filters_infos, input_property_name, database_url):
    def callback(*inputs_values):
        """Update block according to many `inputs_values` corresponding to filters."""
        block_type = block_spec['type']
        block_id = block_spec['id']
        query = block_spec['query']

        resolved_query_params = {}

        # Resolve query params which are filters referenced by pointers.
        filters_blocks_ids = sorted(filters_infos.keys())
        for filter_block_id, input_value in zip(filters_blocks_ids, inputs_values):
            for query_param_name, pointer in filters_infos[filter_block_id].items():
                # Make pointer relative to input value.
                pointer = pointer[len("/" + input_property_name):]
                resolved_query_params[query_param_name] = resolve_pointer(input_value, pointer)

        # Resolve query params which are not filters.
        for query_param_name, query_param in block_spec.get('query_params', {}).items():
            if query_param_name not in resolved_query_params:
                resolved_query_params[query_param_name] = query_param

        db = dataset.connect(database_url)
        result = run_query(db, query, resolved_query_params, block_id)
        block_metadata = blocks.metadata_by_type[block_type]
        result_type = block_metadata["result_type"]
        block_value = prepare_block_value(result, result_type)
        block_options = block_spec.get("options", {})
        render_callback_result_func = block_metadata.get("render_callback_result")
        return render_callback_result_func(block_value, **block_options) \
            if render_callback_result_func is not None \
            else block_value

    return callback


def build_app_layout(title, children):
    return bootstrap.container(fluid=True, children=[
        html.H1(title, className="my-5"),
        html.Div(className="dashboard my-5", children=children),
    ])


def build_errors(title, errors_elements):
    return html.Div(className="alert alert-danger", role="alert", children=[
        html.H4(title, className="alert-heading"),
        html.Ul([
            html.Li(error)
            for error in errors_elements
        ]),
    ])


def create_app(dashboard_file: Path, database_url):
    bootstrap_cdn_css_url = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    app = dash.Dash(__name__)
    app.css.append_css({
        "external_url": bootstrap_cdn_css_url,
    })
    # app.css.config.serve_locally = True

    log.debug("Loading dashboard file %r", str(dashboard_file))
    dashboard_spec, errors = load_dashboard(dashboard_file)

    if errors:
        error = jsonschema.exceptions.best_match(errors)
        error_message = "{}: {}".format("/".join(map(str, error.absolute_path)), error.message)
        log.error("Error loading dashboard file: %s", error_message)
        app.layout = build_app_layout(title=dashboard_spec['title'],
                                      children=[build_errors(title="Error loading dashboard file",
                                                             errors_elements=[error_message])])
        return app

    log.debug("Connecting to database using URL %r", database_url)
    db = dataset.connect(database_url)

    # Load sources.
    sources = dashboard_spec.get('sources', [])
    for source_name, source in sources.items():
        log.debug("Loading data source %r in a table of %r from %r", source_name, db, source)
        load_source(db=db, source=source, target=source_name)

    blocks_specs = dashboard_spec.get('blocks', [])

    # Add default IDs and values to blocks. Do it after indexation to avoid indexing blocks without ID.
    new_blocks_specs = []
    for block_index, block_spec in enumerate(blocks_specs):
        new_block_spec = block_spec.copy()
        if 'id' not in block_spec:
            new_block_spec["id"] = "block-{}-{}".format(block_index, slugify(block_spec["title"]))
        if is_block_filter(block_spec) and 'value' not in block_spec:
            new_block_spec["value"] = get_block_initial_value(block_spec)
        new_blocks_specs.append(new_block_spec)
    blocks_specs = new_blocks_specs

    # Index filters infos. Do it after settings default IDs to blocks.
    # Example: filters_infos = {
    #     "age_filter": {
    #         "min": "/values/0",
    #         "max": "/values/1"
    #     }
    # }
    filter_block_spec_by_id = {}
    filters_infos_by_id = {}
    for block_spec in blocks_specs:
        block_id = block_spec["id"]
        if is_block_filter(block_spec):
            filter_block_spec_by_id[block_id] = block_spec
        else:
            # Check if block query uses one or many filters, and remember their IDs.
            filters_infos = {}
            for query_param_name, query_param in block_spec.get('query_params', {}).items():
                if not isinstance(query_param, str):
                    continue
                match = query_params_reference_regex.match(query_param)
                if match is None:
                    continue
                filter_block_id = match.group(1)
                pointer = match.group(2)
                filters_infos.setdefault(filter_block_id, {})[query_param_name] = pointer
            if filters_infos:
                filters_infos_by_id[block_id] = filters_infos

    # Load blocks. Do it after settings default IDs to blocks.
    blocks_elements_and_cards_classes = []
    for block_index, block_spec in enumerate(blocks_specs, start=1):
        block_id = block_spec["id"]
        log.debug("Loading block %d %r", block_index, block_id)
        filters_infos = filters_infos_by_id.get(block_id)
        block_element = load_block(db, block_spec, filters_infos, filter_block_spec_by_id)
        card_classes = block_spec.get('card_classes')
        blocks_elements_and_cards_classes.append((block_element, card_classes))

    # Set app layout.
    app.layout = build_app_layout(title=dashboard_spec['title'], children=[
        html.Div(className="row", children=[
            html.Div(className="my-3 " + ("col-md-4 col-lg-3" if card_classes is None else card_classes),
                     children=[block_element])
            for block_element, card_classes in blocks_elements_and_cards_classes
        ])
        if blocks_specs
        else html.P("No blocks defined in dashboard file. Start by defining one!"),
    ])

    # Install callbacks. Do it after setting `app.layout`.
    for block_spec in blocks_specs:
        block_id = block_spec['id']
        block_type = block_spec['type']
        block_metadata = blocks.metadata_by_type[block_type]

        # Install internal callbacks needed by to some blocks types.
        for block_callback in block_metadata.get("callbacks", []):
            app.callback(Output('{}__label'.format(block_id), 'children'),
                         [Input(block_id, 'value')],
                         )(block_callback)

        # Install callbacks to update blocks that use one or many filters.
        filters_infos = filters_infos_by_id.get(block_id)
        if filters_infos is not None:
            # Block uses one or many filters, install callback.
            output_property_name = block_metadata["callback_output_component_property"]
            filters_blocks_ids = sorted(filters_infos.keys())
            log.debug("Install callback for block %r on property %r filtered by %r",
                      block_id, output_property_name, filters_blocks_ids)
            inputs = []
            for filter_block_id in filters_blocks_ids:
                filter_block_spec = filter_block_spec_by_id[filter_block_id]
                filter_block_metadata = blocks.metadata_by_type[filter_block_spec['type']]
                input_property_name = filter_block_metadata["callback_input_component_property"]
                inputs.append(Input(filter_block_id, input_property_name))
            app.callback(Output(block_id, output_property_name), inputs)(
                make_callback(block_spec, filters_infos, input_property_name, database_url))

    return app
